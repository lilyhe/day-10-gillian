package com.afs.restapi.service;

import com.afs.restapi.entity.Company;
import com.afs.restapi.exception.CompanyNotFoundException;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.repository.JPACompanyRepository;
import com.afs.restapi.repository.JPAEmployeeRepository;
import com.afs.restapi.service.dto.CompanyRequest;
import com.afs.restapi.service.dto.CompanyResponse;
import com.afs.restapi.service.dto.EmployeeResponse;
import com.afs.restapi.service.mapper.CompanyMapper;
import com.afs.restapi.service.mapper.EmployeeMapper;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CompanyService {

    private final JPACompanyRepository jpaCompanyRepository;

    private final JPAEmployeeRepository jpaEmployeeRepository;

    public CompanyService(JPACompanyRepository jpaCompanyRepository, JPAEmployeeRepository jpaEmployeeRepository) {
        this.jpaCompanyRepository = jpaCompanyRepository;
        this.jpaEmployeeRepository = jpaEmployeeRepository;
    }


    public List<Company> findAll() {
        return jpaCompanyRepository.findAll();
    }

    public List<Company> findByPage(Integer page, Integer size) {
        return jpaCompanyRepository.findAll(PageRequest.of(page - 1, size)).getContent();
    }

    public CompanyResponse findById(Long id) {
        Company company = jpaCompanyRepository.findById(id).orElseThrow(CompanyNotFoundException::new);
        return CompanyMapper.toResponse(company);
    }

    public void update(Long id, CompanyRequest companyRequest) {
        Company toBeUpdatedCompany = jpaCompanyRepository.findById(id).orElseThrow(CompanyNotFoundException::new);
        toBeUpdatedCompany.setName(companyRequest.getName());
        jpaCompanyRepository.save(toBeUpdatedCompany);
    }

    public CompanyResponse create(CompanyRequest companyRequest) {
        Company company = CompanyMapper.toEntity(companyRequest);
        Company savedCompany = jpaCompanyRepository.save(company);
        return CompanyMapper.toResponse(savedCompany);
    }

    public List<EmployeeResponse> findEmployeesByCompanyId(Long id) {
        return EmployeeMapper.toResponses(jpaEmployeeRepository.findByCompanyId(id));
    }

    public void delete(Long id) {
        jpaCompanyRepository.deleteById(id);
    }
}
