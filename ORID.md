# day10-Gillian

## Objective:
Today, we learned DTO(Data Transfer to Object) and how to use Mapper to implement DTO. Besides, we learned the database version control tool of Flyway.
We made a retrospective meeting. I reviewed my learning experience in these two weeks.
We shared cloud-native related topics: Microservices, Container, and CI/CD.

## Reflective:
Relaxing and enjoyable.

## Interpretive:

Mapper makes the use of data in code more rigorous and safer. Retrospective meeting is fun. We can talk freely and get to know each other better.

## Decisional:
Presentation was not good enough on stage. Maybe because of tension I felt I spoke too fast and the logic was not clear enough. I should rehearse it more in advance.










